# Welcome #

This repository contains data processing and analysis code for RNA-Seq analysis of gene expression in pollen tubes.

### Project participants ###

* Rob Reid
* Mark Johnson
* James Pease